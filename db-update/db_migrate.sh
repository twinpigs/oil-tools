#!/bin/bash
SCRIPT_DIRECTORY=$(cd `dirname $0` && pwd)
source $SCRIPT_DIRECTORY/config.sh

cd $PATH_TO_APPLICATION
php yii migrate --interactive=0
return 0