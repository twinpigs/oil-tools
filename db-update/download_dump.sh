#!/bin/bash
SCRIPT_DIRECTORY=$(cd `dirname $0` && pwd)
source $SCRIPT_DIRECTORY/config.sh

chmod 600 $AUTHORIZED_KEYS
echo Скачиваем дамп
scp -i $AUTHORIZED_KEYS $REMOTE_USERNAME@$REMOTE_SERVER:$REMOTE_DUMP_PATH $SCRIPT_DIRECTORY
echo Готово
exit 0