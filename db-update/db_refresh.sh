#!/bin/bash

SCRIPT_DIRECTORY=$(cd `dirname $0` && pwd)
source $SCRIPT_DIRECTORY/config.sh

sudo service postgresql restart
echo Базы разблокированы
sudo -u $POSTGRES_SUPERUSER dropdb $DATABASE
echo База $DATABASE уничтожена
sudo -u $POSTGRES_SUPERUSER createdb $DATABASE
echo База $DATABASE создана
echo Заливаем дамп $SCRIPT_DIRECTORY/dump_last
sudo -u $POSTGRES_SUPERUSER pg_restore -d $DATABASE $SCRIPT_DIRECTORY/dump_last
echo Применяем миграции
cd $PATH_TO_APPLICATION
BRANCH="$(git rev-parse --abbrev-ref HEAD)"
source $SCRIPT_DIRECTORY/db_migrate.sh
echo База обновлена в соответствии с веткой $BRANCH
exit 0