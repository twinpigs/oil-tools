#!/bin/bash

#path in host machine
AUTHORIZED_KEYS=~/PhpstormProjects/eriell-1/tools/authorized_keys
REPOSITORY_ROOT=~/PhpstormProjects/eriell-1/

#remote paths etc.
REMOTE_SERVER=skazkasoft.com
REMOTE_USERNAME=developer
REMOTE_DUMP_PATH=/var/www/db_backups/dump_last

#credentials for server inside virtual machine
POSTGRES_SUPERUSER=postgres
DATABASE=eriell

#pathes inside virtual machine
PATH_TO_APPLICATION=/var/www/html/dev/

#it's how vagrant found that script
REFRESH_SCRIPT_PATH_INSIDE_VAGRANT=/var/www/html/tools/oil-tools/db-update/db_refresh.sh